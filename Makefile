all: effective launch

effective: c/effective.c Makefile
	${CC} -Wall -Wextra -Ofast -o effective c/effective.c
	sudo chgrp onepassword effective
	sudo chmod g+s effective

launch: Makefile
	CGO_ENABLED=0 go build -ldflags "-buildid=" -o launch .

run: launch Makefile
	./launch

.PHONY: launch all run
