package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"syscall"
)

func main() {
	fi, err := os.Stat("./effective")
	if err != nil {
		log.Fatal(err)
	}
	gid := fi.Sys().(*syscall.Stat_t).Gid

	cmd := exec.Command("./effective", fmt.Sprint(gid))

	out, err := cmd.StdoutPipe()
	if err != nil {
		log.Fatal(err)
	}

	err = cmd.Start()
	if err != nil {
		log.Fatal(err)
	}

	output, err := ioutil.ReadAll(out)
	if err != nil {
		log.Fatal(err)
	}

	cmd.Wait()

	log.Println(string(output))
}
