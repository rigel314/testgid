#include <stdio.h>
#include <unistd.h>
#include <errno.h>

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        printf("invalid args\n");
        return 1;
    }
    int targetgid = atoi(argv[1]);
    int err = setgid(targetgid);
    int saveerrno = errno;
    int gid = getegid();
    printf("effective gid: %d, err: %d(%d)\n", gid, err, saveerrno);
    return 0;
}
